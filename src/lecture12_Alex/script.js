var companySlug;
window.onload = () => {
    setCookie('companySlug', 'apetit', 30);
    companySlug = getCookie('companySlug');
    console.log('companySlug: ', companySlug);
    getLanguages();
    getCompany();
}

function getLanguages() {
    $.ajax({
        url: "http://api.coffeein.md/api/languages",
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            const languagesElement = document.getElementById('languages');
            let html = '';
            for (let a = 0; a < response.docs.length; a++) {
                html += `<div class="single-language">${response.docs[a].slug}</div>`
            }
            languagesElement.innerHTML = html;
        },
        error: function () {

        }
    });
}
function getCompany() {
    $.ajax({
        url: `http://api.coffeein.md/api/companies/slug/${companySlug}`,
        type: 'GET',
        dataType: 'json', // added data type
        success: function (response) {
            const element = document.getElementById('company-details');
            let html = `
            <div class="name">Name: ${response.name}</div>
            <div class="phone">Phone: ${response.phone}</div>
            `;
            element.innerHTML = html;
        },
        error: function () {

        }
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
var testString = 'luni,marti,miercuri,joi'
const splittedString = testString.split(',');
