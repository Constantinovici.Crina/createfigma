var company;
window.onload = () => {
    const searchValue = getUrlSearchParam('searchValue');
    const searchElement = document.getElementById('search-input');
    searchElement.value = searchValue;
    // getUrlParam();
    company = getCookie('projectCompany');
    console.log('company by cookie: ', company);
    getCompanyData();
}

function getCompanyData() {
    $.ajax({
        type: "GET",
        url: 'http://api.coffeein.md/api/companies/slug/' + company + '?searchTerm=' + getUrlSearchParam('searchValue'),

        success: function (response) {
            console.log(response);
        },
        dataType: 'json'
    });
}

function getUrlParam() {
    const url = new URL(window.location.href);
    let urlParams = url.pathname.split('/');
    urlParams = urlParams.filter(el => el !== null && el !== '')
    console.log('url: ', urlParams[urlParams.length - 1]);
    company = urlParams[urlParams.length - 1];
}

function setUrlSearchParam(name, value) {
    // console.log(name, value);
    const url = new URL(window.location.href);
    url.searchParams.set(name, value);
    window.location.href = url.href;
}
function getUrlSearchParam(name) {
    const url = new URL(window.location.href);
    const paramValue = url.searchParams.get(name);
    console.log('paramValue: ', paramValue);
    return paramValue;
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
