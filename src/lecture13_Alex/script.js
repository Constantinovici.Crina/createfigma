window.onload = () => {
    const searchValue = getUrlSearchParam('searchValue');
    const searchElement = document.getElementById('search-input');
    searchElement.value = searchValue;
}

function setUrlSearchParam(name, value) {
    // console.log(name, value);
    const url = new URL(window.location.href);
    url.searchParams.set(name, value);
    window.location.href = url.href;
}

function getUrlSearchParam(name) {
    const url = new URL(window.location.href);
    const paramValue = url.searchParams.get(name);
    console.log('paramValue: ', paramValue);
    return paramValue;
}

function goToCompanyPageBySlug(slug) {
    setCookie('projectCompany', slug, 30);
    window.location.href = 'company/index.html';
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
