var company;
window.onload = () => {
    getUrlParam();
    getCompanyData();
}

function getCompanyData() {
    $.ajax({
        type: "GET",
        url: 'http://api.coffeein.md/api/companies/slug/' + company,

        success: function (response) {
            console.log(response);
        },
        dataType: 'json'
    });
}

function getUrlParam() {
    const url = new URL(window.location.href);
    let urlParams = url.pathname.split('/');
    urlParams = urlParams.filter(el => el !== null && el !== '')
    console.log('url: ', urlParams[urlParams.length - 1]);
    company = urlParams[urlParams.length - 1];
}

