var lecture9Script = {};
var requestData;
(function () {
    var today = '11 martie';
    var tomorrow = '12 martie';
    var value = '31'
    var value2 = 45

    function onDocumentReady(test) {
        lecture9Script.value = test;
        lecture9Script.ziuaDeAzi(lecture9Script.value);
        lecture9Script.ziuaDeMaine(11, 13, 15, 17);
    }

// window.onload = () => {
//     ziuaDeAzi();
// }

// function ziuaDeAzi(anul) {
//     console.log('ziua de azi!', anul)
// }

    lecture9Script.ziuaDeMaine = (var1, var2, var3, var4) => {
        console.log('ziua de maine!', tomorrow, var1, var4)
    }

    function acoperis() {

    }

    lecture9Script.ziuaDeAzi = (anul) => {
        console.log('ziua de azi!', anul)
    }

// var ziuaDeAzi = function() {
//     console.log('AAAAAA!')
// }
})();
window.onload = () => {
    document.getElementById('test').addEventListener('click', clicked)
}

function clicked() {
    console.log('CLICKED!')
}

$(document).ready(function () {
//     $('.your-class').slick({
//         'setting-name': setting-value
// });
    getProducts();
    generateSlider();
    $('.your-class').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 300,
        autoplay: true,
        autoplaySpeed: 500,
    });
});

function generateSlider() {
    var items = [
        {
            title: 'title1',
            description: 'description1',
            image: '/assets/images/'
        },
        {
            title: 'title2',
            description: 'description2'
        },
        {
            title: 'title3',
            description: 'description3'
        },
        {
            title: 'title4',
            description: 'description4'
        },
    ]
    const slider = document.getElementById('slider');
    let html = '';
    for (let a = 0; a < items.length; a++) {
        html += `<div class="">
<div class="row">
<div class="description col-lg-6">${items[a].description}</div>
<div class="title col-lg-6">${items[a].title}</div>
</div>
</div>`;
    }
    slider.innerHTML = html;
}

function getProducts() {
    $.ajax({
        type: 'GET',
        url: 'http://api.coffeein.md/api/products/sorted?page=1&searchTerm=&productCategory=&company=coffeein',
        async: true,
        success: (result) => {
            requestData = result;
            console.log('result: ', requestData)
        },
        error: () => {
            console.log('An error occurred!')
        }
    });
}
