function extractTranslatedData(cookieLanguage) {
    $.ajax({
        url: `languages/${cookieLanguage}.json`,
        type: 'GET',
        success: function (serverData) {
            console.log('serverData: ', serverData);
            setSomeHtmlData(serverData);
        },
        error: function () {

        }
    });
}

function setSomeHtmlData(data) {
    const element = document.getElementById('translated-object');
    element.innerHTML = data.COMPLETE_ORDER_1;
}

function getExternButton() {
    $.ajax({
        url: "button.html",
        type: 'GET',
        success: function (button) {
            console.log('button: ', button);
            generateButton(button);
        },
        error: function () {

        }
    });
}
getExternButton();

function generateButton(button) {
    const element = document.getElementById('extracted-html');
    element.innerHTML = button;
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
setCookie('language', 'en', 30)
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getTranslatedDataByCookie() {
    const language = getCookie('language');
    extractTranslatedData(language);
    console.log('language', language);
}
getTranslatedDataByCookie();

function generateRandomLanguage() {
    const languages = ['ro', 'ru', 'en'];
    const randomNumber = Math.floor(Math.random() * 3);
    setCookie('language', languages[randomNumber], 30);
    getTranslatedDataByCookie();
}
